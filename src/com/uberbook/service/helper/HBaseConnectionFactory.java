package com.uberbook.service.helper;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

public class HBaseConnectionFactory {

    private static Configuration _configuration = null;
    private static Path _hbaseConfPath = null;

    private static Path GetHbaseConfPath() {
        if (_hbaseConfPath == null) {
            return new Path(System.getProperty("user.dir") + "/src/com/uberbook/service/configuration/hbase-site.xml");
        } else {
            return _hbaseConfPath;
        }
    }

    public static Connection GetConnectionInstance() throws IOException {
        _configuration = HBaseConfiguration.create();
        _configuration.addResource(GetHbaseConfPath());
        return ConnectionFactory.createConnection(_configuration);
    }

}
