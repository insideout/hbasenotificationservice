package com.uberbook.service.helper;

import java.io.IOException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;

public class TableSetup {

    private Admin _admin = null;

    private static Connection _connection = null;

    public TableSetup() throws IOException {
        _connection = HBaseConnectionFactory.GetConnectionInstance();
        _admin = _connection.getAdmin();
    }

    public void DeleteTable(String tblName) throws IOException {

        HTableDescriptor tableName = new HTableDescriptor(TableName.valueOf(tblName));

        if (_admin.tableExists(tableName.getTableName())) {
            System.out.print("Table exists, Deleting...");
            _admin.disableTable(tableName.getTableName());
            _admin.deleteTable(tableName.getTableName());
            System.out.println(" Done.");
        }
        else {
            System.out.println("Table does not exist. Nothing to delete.");
        }
    }

    public void CreateTable(String tblName, String[] columnFamilies, boolean dropIfExist) throws IOException {

        if (dropIfExist) {
            DeleteTable(tblName);
        }

        HTableDescriptor tableName = new HTableDescriptor(TableName.valueOf(tblName));

        for (String cf : columnFamilies) {
            tableName.addFamily(new HColumnDescriptor(cf));
        }

        if (!_admin.tableExists(tableName.getTableName())) {
            System.out.print("Creating table.");
            _admin.createTable(tableName);
            System.out.println(" Done.");
        }
    }

}
