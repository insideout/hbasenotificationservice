package com.uberbook.service.notification.NotificationType;

import com.uberbook.service.notification.Notification;
import com.uberbook.service.notification.Column;

public class PMNotification extends Notification{

    NotificationType type = NotificationType.PRIVATE_MESSAGE;

    public PMNotification(String fromUser, String user, String pmText) {
        this.columns.add(new Column("attributes", "type", this.type.toString()));
        this.columns.add(new Column("attributes", "from_user", fromUser));
        this.columns.add(new Column("attributes", "for_user", user));
        this.columns.add(new Column("attributes", "text", pmText));
    }

    public String toString(){
        return "You have a private message from " + this.columns.get(1).getValue()
                + " '" + this.columns.get(3).getValue() + "'";
    }

}
