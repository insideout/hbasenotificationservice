package com.uberbook.service.notification.NotificationType;

import com.uberbook.service.notification.Notification;
import com.uberbook.service.notification.Column;

public class CommentNotification extends Notification {

    NotificationType type = NotificationType.COMMENT;

    public CommentNotification(String fromUser, String user, String commentText,
                        ResourceType commentedOn, String commentedOnURL)
    {
        this.columns.add(new Column("attributes", "type", this.type.toString()));
        this.columns.add(new Column("attributes", "from_user", fromUser));
        this.columns.add(new Column("attributes", "for_user", user));
        this.columns.add(new Column("attributes", "commentedOn", commentedOn.toString()));
        this.columns.add(new Column("attributes", "text", commentText));
        this.columns.add(new Column("attributes", "url", commentedOnURL));

    }

    public String toString(){

        return this.columns.get(1).getValue() + " commented on your " +
                this.columns.get(3).getValue() + ": '" +
                this.columns.get(4).getValue() + "'";
    }

}
