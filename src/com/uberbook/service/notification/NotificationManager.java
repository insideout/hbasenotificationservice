package com.uberbook.service.notification;

import com.uberbook.service.helper.HBaseConnectionFactory;
import com.uberbook.service.helper.TableSetup;
import com.uberbook.service.notification.NotificationType.LikeNotification;
import com.uberbook.service.notification.NotificationType.CommentNotification;
import com.uberbook.service.notification.NotificationType.FriendRequestNotification;
import com.uberbook.service.notification.NotificationType.PMNotification;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.List;
import java.util.ArrayList;
import java.util.NavigableMap;
import java.util.HashMap;

public class NotificationManager {

    private static Connection _connection = null;

    public NotificationManager() throws IOException {

        // Setup the notification table
        TableSetup setup = new TableSetup();
        setup.CreateTable("notifications", new String[] {"attributes", "metrics"}, true);

        _connection = HBaseConnectionFactory.GetConnectionInstance();
    }

    public Notification createNotification(Notification.NotificationType type, Map<String, String> parameters) {

        switch(type) {
            case LIKE:
                return new LikeNotification(
                        parameters.get("from_user"),
                        parameters.get("for_user"),
                        Notification.ResourceType.valueOf(parameters.get("liked")),
                        parameters.get("url"));

            case COMMENT:
                return new CommentNotification(
                        parameters.get("from_user"),
                        parameters.get("for_user"),
                        parameters.get("text"),
                        Notification.ResourceType.valueOf(parameters.get("commentedOn")),
                        parameters.get("url"));

            case PRIVATE_MESSAGE:
                return new PMNotification(
                        parameters.get("from_user"),
                        parameters.get("for_user"),
                        parameters.get("text"));

            case FRIEND_REQUEST:
                return new FriendRequestNotification(
                        parameters.get("from_user"),
                        parameters.get("for_user"));
            default:
                return new Notification();
        }
    }

    public void addNotification(Notification notification) throws IOException {

        Table table = _connection.getTable(TableName.valueOf("notifications"));

        String uniqueID = UUID.randomUUID().toString();

        Put put = new Put(Bytes.toBytes(uniqueID));

        List<Column> columns=notification.getColumns();

        for(Column column:columns){

            put.addColumn(Bytes.toBytes(column.getColumnFamily()),
                    Bytes.toBytes(column.getColumnName()),
                    Bytes.toBytes(column.getValue()));
        }

        table.put(put);
    }

    public List<Notification> getUserNotifications(String user) throws Exception {

        List<Notification> userNotifications = new ArrayList<>();

        Table table = _connection.getTable(TableName.valueOf("notifications"));

        SingleColumnValueFilter filter = new SingleColumnValueFilter(
                Bytes.toBytes("attributes"),
                Bytes.toBytes("for_user"),
                CompareFilter.CompareOp.EQUAL,
                new BinaryComparator(Bytes.toBytes(user)));

        filter.setFilterIfMissing(true);
        Scan userScan = new Scan();
        userScan.setFilter(filter);
        userScan.addFamily(Bytes.toBytes("attributes"));
        ResultScanner userScanResult = table.getScanner(userScan);
        for (Result res : userScanResult) {

            Notification.NotificationType type = Notification.NotificationType.valueOf(
                    Bytes.toString(res.getValue(Bytes.toBytes("attributes"), Bytes.toBytes("type"))));

            Map<String,String> parameters = parseResults(res);

            userNotifications.add(createNotification(type, parameters));
        }
        userScanResult.close();
        return  userNotifications;
    }

    private Map<String,String> parseResults(Result result){

        Map<String,String> parameters = new HashMap<>();

        NavigableMap<byte[],
                NavigableMap<byte[],byte[]>> resultMap = result.getNoVersionMap();

        for (byte[] columnFamily : resultMap.keySet()) {
            NavigableMap<byte[], byte[]> columnMap = resultMap.get(columnFamily);
            for (byte[] column : columnMap.keySet()) {
                String col = Bytes.toString(column);
                String value = Bytes.toString(columnMap.get(column));
                if(col != "type") {
                    parameters.put(col, value);
                }
            }
        }

        return parameters;
    }
}