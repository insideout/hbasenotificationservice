package com.uberbook.service.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationApp {

    public static void main(String[] args) throws Exception {

        Map<String,String> pm1 = new HashMap<>();
        pm1.put("from_user", "Mark Zuckerberg");
        pm1.put("for_user", "Barack Obama");
        pm1.put("text", "Hey how are you, fancy for a drink?");

        Map<String,String> pm2 = new HashMap<>();
        pm2.put("from_user", "Barack Obama");
        pm2.put("for_user", "Mark Zuckerberg");
        pm2.put("text", "I can't, playing golf with Trump today.");

        Map<String,String> fr1 = new HashMap<>();
        fr1.put("from_user", "Barack Obama");
        fr1.put("for_user", "Mark Zuckerberg");

        Map<String,String> fr2 = new HashMap<>();
        fr2.put("from_user", "Donald Trump");
        fr2.put("for_user", "Luke Skywalker");

        Map<String,String> like = new HashMap<>();
        like.put("from_user", "Barack Obama");
        like.put("for_user", "Donald Trump");
        like.put("liked", "LINK");
        like.put("url","");

        Map<String,String> comment = new HashMap<>();
        comment.put("from_user", "Donald Trump");
        comment.put("for_user", "Hillary Clinton");
        comment.put("text","Looking good tonight babe!");
        comment.put("commentedOn", "PHOTO");
        comment.put("url","");

        NotificationManager manager = new NotificationManager();
        List<Notification> notifications = new ArrayList<>();

        notifications.add(manager.createNotification(Notification.NotificationType.PRIVATE_MESSAGE, pm1));
        notifications.add(manager.createNotification(Notification.NotificationType.PRIVATE_MESSAGE, pm2));
        notifications.add(manager.createNotification(Notification.NotificationType.LIKE, like));
        notifications.add(manager.createNotification(Notification.NotificationType.COMMENT, comment));
        notifications.add(manager.createNotification(Notification.NotificationType.FRIEND_REQUEST, fr1));
        notifications.add(manager.createNotification(Notification.NotificationType.FRIEND_REQUEST, fr2));

        for (Notification notification:notifications){
            manager.addNotification(notification);
        }

        System.out.println();

        System.out.println("Getting notification for Mark");
        List<Notification> markNotifications = manager.getUserNotifications("Mark Zuckerberg");

        for (Notification notification:markNotifications) {
            System.out.println(notification.toString());
        }

        System.out.println();

        System.out.println("Getting notification for Barack");
        List<Notification> barackNotifications = manager.getUserNotifications("Barack Obama");

        for (Notification notification:barackNotifications) {
            System.out.println(notification.toString());
        }

        System.out.println();

        System.out.println("Getting notification for Hillary");
        List<Notification> hillaryNotifications = manager.getUserNotifications("Hillary Clinton");

        for (Notification notification:hillaryNotifications) {
            System.out.println(notification.toString());
        }

    }
}
